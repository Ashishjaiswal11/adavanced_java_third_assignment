package com.nagarro.serviceimpl;

import java.util.List;

import com.nagarro.dao.ProductDao;
import com.nagarro.daoimpl.ProductDaoImpl;
import com.nagarro.models.Product;
import com.nagarro.service.ProductService;

//ProductService interface implementation
public class ProductServiceImpl implements ProductService {
	private final ProductDao pd;

	// ProductDaoImpl initialization
	public ProductServiceImpl() {
		this.pd = new ProductDaoImpl();
	}

	@Override
	public int addProduct(Product product) {
		return pd.addProduct(product);
	}

	@Override
	public List<Product> getAllProduct() {
		return pd.getAllProduct();
	}

	@Override
	public Product getProduct(int productId) {
		return pd.getProduct(productId);
	}

	@Override
	public int updateProduct(Product product) {
		return pd.updateProduct(product);
	}

	@Override
	public int deleteProduct(int productId) {
		return pd.deleteProduct(productId);
	}

}
