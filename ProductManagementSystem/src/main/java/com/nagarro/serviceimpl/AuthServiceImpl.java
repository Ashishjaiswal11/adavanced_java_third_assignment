package com.nagarro.serviceimpl;

import com.nagarro.dao.AuthDao;
import com.nagarro.daoimpl.AuthDaoImpl;
import com.nagarro.models.User;
import com.nagarro.service.AuthService;

//AuthService interface Implementation
public class AuthServiceImpl implements AuthService {
	private final AuthDao authDao;

	public AuthServiceImpl() {
		this.authDao = new AuthDaoImpl();
	}

	@Override
	public boolean isAuthUser(User user) {
		return authDao.isAuthUser(user);
	}

}
