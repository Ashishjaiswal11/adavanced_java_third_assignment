package com.nagarro.service;

import com.nagarro.models.User;

//interface for verify user
public interface AuthService {

	boolean isAuthUser(User user);
}