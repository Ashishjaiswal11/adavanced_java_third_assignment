package com.nagarro.dao;

import java.util.List;

import com.nagarro.models.Product;

//interface for product related methods
public interface ProductDao {
	// for adding product
	int addProduct(Product product);

	// for getting all product list
	List<Product> getAllProduct();

	// get any specific record
	Product getProduct(int productId);

	// for update any record
	int updateProduct(Product product);

	// delete any record
	int deleteProduct(int productId);
}
