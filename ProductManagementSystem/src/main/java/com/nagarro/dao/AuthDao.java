package com.nagarro.dao;

import com.nagarro.models.User;

//interface for verfication valid user  or not
public interface AuthDao {
	boolean isAuthUser(User user);
}
