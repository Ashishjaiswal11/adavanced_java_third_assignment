package com.nagarro.models;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

//product entity class for product list management in  database
@Entity
@Table(name = "products")
public class Product {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "pId", nullable = false)
	private int productId;

	@Column(name = "pTitle")
	private String productTitle;

	@Column(name = "pQuantity")
	private int productQuantity;

	@Column(name = "pSize")
	private int productSize;

	@Column(name = "pImage")
	private String productImage;

	public Product() {
	}

	public int getProductId() {
		return productId;
	}

	public void setProductId(int productId) {
		this.productId = productId;
	}

	public String getProductTitle() {
		return productTitle;
	}

	public void setProductTitle(String productTitle) {
		this.productTitle = productTitle;
	}

	public int getProductQuantity() {
		return productQuantity;
	}

	public void setProductQuantity(int productQuantity) {
		this.productQuantity = productQuantity;
	}

	public int getProductSize() {
		return productSize;
	}

	public void setProductSize(int productSize) {
		this.productSize = productSize;
	}

	public String getProductImage() {
		return productImage;
	}

	public void setProductImage(String productImage) {
		this.productImage = productImage;
	}

	public Product(int productId, String productTitle, int productQuantity, int productSize, String productImage) {
		super();
		this.productId = productId;
		this.productTitle = productTitle;
		this.productQuantity = productQuantity;
		this.productSize = productSize;
		this.productImage = productImage;
	}

	// override to string for custom output
	@Override
	public String toString() {
		return "Product [productId=" + productId + ", productTitle=" + productTitle + ", productQuantity="
				+ productQuantity + ", productSize=" + productSize + ", productImage=" + productImage + "]";
	}

}
