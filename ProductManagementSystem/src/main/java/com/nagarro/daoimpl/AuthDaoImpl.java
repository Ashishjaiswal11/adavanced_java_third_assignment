package com.nagarro.daoimpl;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import com.nagarro.dao.AuthDao;
import com.nagarro.models.User;
import com.nagarro.utils.HibernateUtil;

//interface implementation for user verification
public class AuthDaoImpl implements AuthDao {
	@Override
	public boolean isAuthUser(User user) {
		try (Session session = HibernateUtil.getSessionFactory().openSession()) {
			Transaction t = session.beginTransaction();
			String query = "from User";
			Query q = session.createQuery(query);
			List<User> ad = q.list();
			for (User obj : ad) {
				if (obj.getUsername().equals(user.getUsername()) && obj.getPassword().equals(user.getPassword())) {
					session.getTransaction().commit();
					session.close();
					return true;

				}
			}

		}
		return false;
	}
}
