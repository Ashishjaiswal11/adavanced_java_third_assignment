package com.nagarro.controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.nagarro.models.User;
import com.nagarro.service.AuthService;
import com.nagarro.serviceimpl.AuthServiceImpl;

// implementation of Servlet
public class AuthController extends HttpServlet {
	private static final long serialVersionUID = 1023239L;

	public AuthController() {
		super();
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("text/html");
		User checkUsr = new User();
		String user = request.getParameter("username");
		String password = request.getParameter("password");
		checkUsr.setUsername(user);
		checkUsr.setPassword(password);
		AuthService service = new AuthServiceImpl();
		if (service.isAuthUser(checkUsr)) {
			HttpSession session = request.getSession();
			session.setAttribute("username", user);
			response.sendRedirect("./Products");
		} else {
			try (PrintWriter out = response.getWriter()) {
				out.println("<!DOCTYPE html>");
				out.println("<html>");
				out.println("<head>");
				out.println("<meta charset=\"utf-8\">"); // escape the quote marks
				out.println("<title>Invalid Username/Password</title>");
				out.println("<style>"); // start style
				// enclose style attributes withing the <style> </style> elements
				out.println("h5 {"); // note leading brace
				out.println("color:blue;");
				out.println("font-size:12px;");
				out.println("align:center;");
				out.println("display:inline;");
				out.println("margin-left:420px;");
				out.println("background-color:red;");
				out.println("border: 1px solid black;");
				out.println("}"); // note trailing brace for h1 style
				// add styles for other elements here using similar structure
				// note that separate lines are used for clarity -
				// all of the above could be one println
				out.println("</style>"); // terminate style
				out.println("</head>");
				out.println("<body>");
				out.print("<h5>You have entered an invalid username or password</h5>");
				out.println("</body>");
				out.println("</html>");

				RequestDispatcher rs = request.getRequestDispatcher("index.jsp");
				rs.include(request, response);

			}

		}

	}
}