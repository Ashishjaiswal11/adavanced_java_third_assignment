package com.nagarro.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;

import com.nagarro.models.Product;
import com.nagarro.service.ProductService;
import com.nagarro.serviceimpl.ProductServiceImpl;

//Manage request related to product 
@MultipartConfig
public class ProductController extends HttpServlet {
	private static final long serialVersionUID = 11233122L;
	private final ProductService ps;

	public ProductController() {
		this.ps = new ProductServiceImpl();
	}

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String path = request.getServletPath();
		switch (path) {
		case "/Products/editProduct":
			editProduct(request, response);
			break;
		case "/Products/deleteProduct":
			deleteProduct(request, response);
			break;
		default:
			break;
		}

	}

	private void editProduct(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();
		int id = Integer.parseInt(request.getParameter("id"));
		Product product = ps.getProduct(id);
		session.setAttribute("editProductDetail", product);
		System.out.println("Asas");
		RequestDispatcher rd = request.getRequestDispatcher("./edit");
		rd.forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String path = request.getServletPath();
		switch (path) {
		case "/Products/addProduct":
			addProduct(request, response);
			break;
		case "/Products/getAllProduct":
			getAllProduct();
			break;
		case "/Products/updateProduct":
			updateProduct(request, response);
			break;
		default:
			break;
		}
	}

	private void updateProduct(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		HttpSession session = request.getSession();
		String path = uploadImgFile(request);
		Product edit = (Product) session.getAttribute("editProductDetail");
		Product product = new Product();
		product.setProductId(edit.getProductId());
		product.setProductTitle(request.getParameter("title").trim());
		product.setProductSize(Integer.parseInt(request.getParameter("size").trim()));
		product.setProductQuantity(Integer.parseInt(request.getParameter("quantity").trim()));
		product.setProductImage(path);
		ps.updateProduct(product);
		response.sendRedirect("../");
	}

	public List<Product> getAllProduct() {
		return ps.getAllProduct();
	}

	private void deleteProduct(HttpServletRequest request, HttpServletResponse response) throws IOException {
		int id = Integer.parseInt(request.getParameter("id"));
		ps.deleteProduct(id);
		response.sendRedirect("../");
	}

	private void addProduct(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		String path = uploadImgFile(request);
		Product product = new Product();
		product.setProductTitle(request.getParameter("title").trim());
		product.setProductSize(Integer.parseInt(request.getParameter("size").trim()));
		product.setProductQuantity(Integer.parseInt(request.getParameter("quantity").trim()));
		product.setProductImage(path);
		ps.addProduct(product);
		response.sendRedirect("../Products");
	}

	private String uploadImgFile(HttpServletRequest request) throws IOException, ServletException {
		Part filePart = request.getPart("image");
		String fileName = filePart.getSubmittedFileName();
		for (Part part : request.getParts()) {
			part.write(
					"C:\\Users\\ashishjaiswal\\eclipse-workspace\\ProductManagementSystem\\src\\main\\webapp\\images\\"
							+ fileName);
		}
		return (fileName);
	}
}